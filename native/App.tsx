import React, { useEffect, useState } from 'react'
import * as Font from 'expo-font'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { AppState, INIT } from 'cupidon-common/state'
import Loader from './components/molecules/loader'
import Alerts from './components/molecules/alerts'
import { ThemeProvider } from 'react-native-elements'
import Navigation from './components/organisms/navigation'
import { AppTheme } from './app-theme'
import Notification from './components/molecules/notification'
import Geolocation from './components/molecules/geolocation'

const store = createStore(AppState)
store.dispatch({ type: INIT, env: process.env.REACT_APP_ENV_NAME })
// store.dispatch({ type: INIT, env: 'production' }) // Must be enabled for production release

export default function App() {
  const [ isLoaded, setIsLoaded ] = useState<boolean>(false)

  useEffect(() => {
    async function loadResourcesAndDataAsync() {
      await Font.loadAsync({
        'montserrat': require('./assets/fonts/Montserrat-Regular.ttf'),
        'montserrat-semi-bold': require('./assets/fonts/Montserrat-SemiBold.ttf'),
        'montserrat-medium': require('./assets/fonts/Montserrat-Medium.ttf')
      })
      setIsLoaded(true)
    }
    loadResourcesAndDataAsync()
  }, [])

  return (
    <Provider store={store}>
      {
        isLoaded ?
          <SafeAreaProvider>
            <ThemeProvider theme={ AppTheme.RNE }>
              <Loader />
              <Alerts />
              <Navigation />
              <Notification />
              <Geolocation />
            </ThemeProvider>
          </SafeAreaProvider>
          :
          <Loader />
      }
    </Provider>
  )
}
