import { Theme } from '@react-navigation/native/src/types'
import { StyleSheet } from 'react-native'
import { Theme as RNETheme } from 'react-native-elements'

const colors = {
  theme: 'rgb(244, 81, 30)',
  pink: 'rgba(221,60,115,1)',
  pinkLight: 'rgba(244,97,97,1)',
  black: 'rgb(34,34,34)',
  blackLight: 'rgb(68,68,68)',
  blackLighter: 'rgb(88,88,88)',
  grey: 'rgb(135,135,135)',
  greyLight: 'rgb(153,153,153)',
  greyLighter: 'rgb(237,237,237)',
  white: 'rgb(255, 255, 255)',
  whiteDark: 'rgb(250, 250, 250)',
  whiteDarker: 'rgb(242,242,242)',
}

const navigationContainer: Theme = {
  dark: false,
  colors: {
    primary: 'rgb(0, 122, 255)',
    background: colors.greyLighter,
    card: colors.white,
    text: colors.black,
    border: colors.whiteDark,
    notification: 'rgb(255, 59, 48)',
  }
}

const CommonElements = StyleSheet.create({
  Title: {
    color: colors.white
  },
  Text: {
    color: colors.blackLighter,
    fontFamily: 'montserrat-medium'
  },
  Shadow: {
    shadowColor: colors.black,
    shadowOffset:{
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.50,
    shadowRadius: 4.65,
    elevation: 8,
  }
})

const RNE: RNETheme = {
  Button: {
    buttonStyle: {
      backgroundColor: 'transparent'
    },
    titleStyle: {
      fontFamily: 'montserrat-medium'
    }
  },
  Card: {
    containerStyle: {
      ...CommonElements.Shadow,
      flex: 1,
      borderRadius: 15,
      backgroundColor: colors.white
    },
  },
  CardTitle: {
    style: {
      fontSize: 18,
      color: colors.blackLight,
      fontFamily: 'montserrat-semi-bold'
    }
  },
  CheckBox: {
    checkedColor: colors.pink,
    containerStyle: { backgroundColor: 'transparent', marginLeft: 0, borderWidth: 0 },
    textStyle: { color: colors.greyLight }
  },
  Divider: {
    style: {
      backgroundColor: colors.whiteDark,
      marginBottom: 15
    }
  },
  Icon: {
    color: colors.greyLight
  },
  Input: {
    inputStyle: {
      color: colors.blackLight
    }
  },
  Text: {
    style: CommonElements.Text,
    h1Style: {
      color: colors.blackLight,
      fontFamily: 'montserrat-medium'
    }
  }
}

export const AppTheme = {
  colors: colors,
  navigationContainer: navigationContainer,
  RNE: RNE
}
