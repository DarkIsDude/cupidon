import React from 'react'
import { Button, Card, CheckBox, Input, Text } from 'react-native-elements'
import { connect } from 'react-redux'
import { error, loaded, loading, State, API as IAPI, success } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import { NavigationProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import RootStackParamList from '../route'
import { ScrollView, View } from 'react-native'
import { AppTheme } from '../app-theme'
import { LinearGradient } from 'expo-linear-gradient'

interface Props extends NavigationProp<{}> {
  api: IAPI
  loading: () => void
  loaded: () => void
  error: (message: string) => {}
  success: (message: string) => {}
  navigation: StackNavigationProp<RootStackParamList, 'SignIn'>
}

function InternalSignIn(props: Props) {
  const { navigation, loading, loaded, error, success, api } = props
  const [ email, setEmail ] = React.useState<string>()
  const [ password, setPassword ] = React.useState<string>()
  const [ passwordConfirm, setPasswordConfirm ] = React.useState<string>()
  const [ prefix, setPrefix ] = React.useState<string>('M')
  const [ prefixIndex, setPrefixIndex ] = React.useState<number>(0)
  const [ firstname, setFirstname ] = React.useState<string>()
  const [ lastname, setLastname ] = React.useState<string>()
  const [ phone, setPhone ] = React.useState<string>()

  const [ passwordMissmatch, setPasswordMissmatch ] = React.useState<boolean>()
  const [ emptyField, setEmptyField ] = React.useState<boolean>()

  const signup = async () => {
    if (password !== passwordConfirm) {
      return setPasswordMissmatch(true)
    }

    setPasswordMissmatch(false)
    if (!email || !password || !prefix || !firstname || !lastname || !phone) {
      return setEmptyField(true)
    }

    setEmptyField(false)
    loading()

    try {
      await api.signup(email, password, prefix, firstname, lastname, phone)
      success('Bienvenu !')
      navigation.navigate('Login')
    } catch (e) {
      console.error(e)

      if (e.isAxiosError && e.response) {
        const keys = Object.keys(e.response.data.error)
        const text = e.response.data.error[ keys[ 0 ] ][ 0 ]
        error(`Erreur lors de votre inscription: ${text}`)
      } else {
        error('Erreur lors de votre inscription')
      }
    } finally {
      loaded()
    }
  }

  return (
    <ScrollView>
      <View style={{ padding: 30 }}>
        <View style={{ marginBottom: 30 }}>
          <Text h1>Créer un compte</Text>
        </View>
        <View style={{ marginBottom: 15 }}>
          <Text h4>Informations de connexion</Text>
        </View>
        <View style={{ marginBottom: 15 }}>
          <Input
            placeholder='Email'
            onChangeText={setEmail}
            errorMessage={emptyField && !email ? 'Erreur' : ''}
          />
          <Input
            secureTextEntry={true}
            onChangeText={setPassword}
            placeholder='Mot de passe'
            errorMessage={passwordMissmatch || (emptyField && !password) ? 'Erreur' : ''}
          />
          <Input
            secureTextEntry={true}
            onChangeText={setPasswordConfirm}
            placeholder='Confirmation mot de passe'
            errorMessage={passwordMissmatch ? 'Erreur' : ''}
          />
        </View>

        <View style={{ marginBottom: 15 }}>
          <Text h4>Informations personnelles</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <CheckBox
            center
            title='M'
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checked={prefixIndex === 0}
            onPress={() => { setPrefix('M'); setPrefixIndex(0) }}
          />
          <CheckBox
            center
            title='Mme'
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checked={prefixIndex === 1}
            onPress={() => { setPrefix('Mme'); setPrefixIndex(1) }}
          />
        </View>

        <Input
          placeholder='Prénom'
          onChangeText={setFirstname}
          errorMessage={emptyField && !firstname ? 'Erreur' : ''}
        />
        <Input
          placeholder='Nom'
          onChangeText={setLastname}
          errorMessage={emptyField && !lastname ? 'Erreur' : ''}
        />
        <Input
          placeholder='Téléphone'
          onChangeText={setPhone}
          errorMessage={emptyField && !phone ? 'Erreur' : ''}
        />

        <View style={{ marginBottom: 60 }}>
          <LinearGradient colors={[ AppTheme.colors.pinkLight, AppTheme.colors.pink ]} style={{ borderRadius: 15 }}>
            <Button title="Inscription" onPress={signup} />
          </LinearGradient>
        </View>

        <Card.Divider />

        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <Text>Déjà un compte ? </Text>
          <Text onPress={() => navigation.navigate('Login')} style={{ color: AppTheme.colors.black, textDecorationLine:'underline' }}>Connexion</Text>
        </View>

      </View>
    </ScrollView>
  )
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    success: (message: string) => dispatch(success(message)),
    error: (message: string) => dispatch(error(message)),
  }
}

const SignIn = connect(mapStateToProps, mapDispatchToProps)(InternalSignIn)

export default SignIn
