import React from 'react'
import { Button, Divider, Input, Text } from 'react-native-elements'
import { connect } from 'react-redux'
import { error, loaded, loading, State, API as IAPI, success, User, login } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import { NavigationProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import RootStackParamList from '../route'
import { ScrollView, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { AppTheme } from '../app-theme'

interface Props extends NavigationProp<{}> {
  api: IAPI
  loading: () => void
  loaded: () => void
  error: (message: string) => void
  success: (message: string) => void
  login(user: User, api: IAPI): void;
  navigation: StackNavigationProp<RootStackParamList, 'Login'>
}

function InternalLogin(props: Props) {
  const { loading, loaded, error, api, navigation, success, login } = props
  const [ email, setEmail ] = React.useState<string>('')
  const [ password, setPassword ] = React.useState<string>('')
  const [ loginError, setLoginError ] = React.useState<boolean>(false)

  const handleLogin = async () => {
    if (!email || !password) {
      return setLoginError(true)
    }

    try {
      loading()
      const newAPI = await api.login(email, password)
      const user = await newAPI.me()
      success('Connecté')
      login(user, newAPI)
      navigation.goBack()
    } catch (e) {
      console.error(e)
      error('Erreur lors de votre connexion')
      setLoginError(true)
    } finally {
      loaded()
    }
  }

  return (
    <ScrollView>
      <View style={{ padding: 30 }}>
        <View style={{ marginBottom: 15 }}>
          <Text h1>Connexion</Text>
        </View>
        <View>
          <Input
            placeholder='Adresse email'
            onChangeText={setEmail}
            errorMessage={loginError ? 'Connexion impossible' : ''}
          />
          <Input
            secureTextEntry={true}
            placeholder='Mot de passe'
            onChangeText={setPassword}
          />
        </View>
        <View style={{ marginBottom: 60 }}>
          <LinearGradient colors={[ AppTheme.colors.pinkLight, AppTheme.colors.pink ]} style={{ borderRadius: 15 }}>
            <Button onPress={handleLogin} title="Connexion" />
          </LinearGradient>
        </View>
        <Divider />
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <Text>Pas de compte ? </Text>
          <Text onPress={() => navigation.navigate('SignIn')} style={{ color: AppTheme.colors.black, textDecorationLine:'underline' }}>Inscription</Text>
        </View>
      </View>
    </ScrollView>
  )
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    login: (user: User, api: IAPI) => dispatch(login(user, api)),
    success: (message: string) => dispatch(success(message)),
    error: (message: string) => dispatch(error(message)),
  }
}

const Login = connect(mapStateToProps, mapDispatchToProps)(InternalLogin)

export default Login
