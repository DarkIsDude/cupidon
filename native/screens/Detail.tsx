import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, View } from 'react-native'
import { RouteProp } from '@react-navigation/native'
import RootStackParamList from '../route'
import { error, loaded, loading, Offer, State, success, API, User, Geolocation } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Button, Card, Divider, Icon, Text } from 'react-native-elements'
import { StackNavigationProp } from '@react-navigation/stack'
import { AppTheme } from '../app-theme'
import OfferDistance from '../components/atoms/offer-distance'
import { LinearGradient } from 'expo-linear-gradient'
import OfferAddress from '../components/atoms/offer-address'

type ProfileScreenRouteProp = RouteProp<RootStackParamList, 'Detail'>

const style = StyleSheet.create({
  item: {
    flexDirection: 'row',
    marginBottom: 15,
    fontSize: 15
  },
  icon: {
    width: 30
  }
})

interface Props {
  route: ProfileScreenRouteProp
  api: API
  geolocation: Geolocation
  user?: User
  loaded: () => void
  loading: () => void
  error: (message: string) => {}
  success: (message: string) => {}
  navigation: StackNavigationProp<RootStackParamList, 'Home'>
}

function InternalDetail(props: Props) {
  const { loading, loaded, error, api, navigation, user, success, geolocation } = props
  const { id } = props.route.params
  const [ offer, setOffer ] = useState<Offer|null>()

  useEffect(() => {
    const loadOffer = async () => {
      try {
        loading()
        setOffer(await api.offer(id, geolocation))
      } catch (e) {
        error('Erreur')
        console.error(e)
      } finally {
        loaded()
      }
    }

    loadOffer()
  }, [ api, loading, loaded, error, id, geolocation ])

  if (!offer) {
    return <></>
  }

  const toggleApply = async () => {
    if (!user) {
      navigation.navigate('Login')
    }

    try {
      loading()

      if (offer.auth_user_applied) {
        await api.detach(offer)
        success('Votre participation a été retiré')
      } else {
        await api.attach(offer)
        success('Votre participation a été enregistré')
      }

      offer.auth_user_applied = !offer.auth_user_applied
      setOffer(offer)
    } catch (e) {
      error(`${offer.name} : Impossible de contacter le server`)
      console.error(e)
    } finally {
      loaded()
    }
  }

  return (
    <ScrollView>
      <View style={{ padding: 15 }}>
        <View style={{ marginBottom: 30 }}>
          <Text h1>{ offer.name }</Text>
        </View>

        <Card containerStyle={{ marginBottom: 30 }}>
          <View style={style.item}>
            <Icon name={'insert-invitation'} size={20} iconStyle={style.icon} />
            <Text style={{ fontWeight: 'bold', fontSize: 15 }}>{offer.start.toDateString() } ({ offer.durationToString() })</Text>
          </View>
          <View style={style.item}>
            <Icon name={'place'} size={20} iconStyle={style.icon} />
            <OfferAddress offer={offer} />
          </View>
          <OfferDistance offer={offer} />
          <View style={{ ...style.item, marginBottom: 0 }}>
            <Icon name={'people'} size={20} iconStyle={style.icon} />
            <Text>{offer.answers_count} participant{offer.answers_count > 1 ? 's' : ''}</Text>
          </View>
        </Card>

        <View style={style.item}>
          <Text style={{ textAlign: 'justify', color: AppTheme.colors.grey }}>{offer.description}</Text>
        </View>

        <Divider />

        <View style={[ style.item, { flexDirection: 'column' } ]}>
          <Text style={{ fontWeight: 'bold' }}>Responsable de l'offre</Text>
          <Text style={{ marginLeft: 15, textTransform: 'capitalize' }}>{offer.user.prefix} {offer.user.firstname} {offer.user.lastname}</Text>
          <Text style={{ marginLeft: 15 }}>{ offer.establishment.name }</Text>
        </View>

        <Divider />

        <View style={{ marginBottom: 15 }}>
          <LinearGradient colors={[ AppTheme.colors.pinkLight, AppTheme.colors.pink ]} style={{ borderRadius: 15 }}>
            <Button
              disabled={user && !offer.auth_user_can_apply}
              onPress={ toggleApply }
              title={ offer.auth_user_can_apply ? (offer.auth_user_applied ? 'Ne plus participer à la mission' : 'Participer à la mission') : 'Vous ne pouvez pas participer à cette mission'} />
          </LinearGradient>
        </View>

      </View>
    </ScrollView>
  )
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
    geolocation: state.geolocation,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    error: (message: string) => dispatch(error(message)),
    success: (message: string) => dispatch(success(message)),
  }
}

const Detail = connect(mapStateToProps, mapDispatchToProps)(InternalDetail)

export default Detail
