import React, { useEffect, useState } from 'react'
import { ScrollView, View } from 'react-native'
import { Text } from 'react-native-elements'
import { connect } from 'react-redux'
import { error, loaded, loading, State, API as IAPI, Pagination, OfferLight, Geolocation } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import CardList from '../components/organisms/card-list'
import { AppTheme } from '../app-theme'

interface Props {
  api: IAPI
  geolocation: Geolocation
  loading: () => void
  loaded: () => void
  error: (message: string) => {}
}

function InternalHome(props: Props) {
  const { loading, loaded, error, api, geolocation } = props
  const [ pagination, setPagination ] = useState<Pagination<OfferLight>>({ current_page: 1, data: [], last_page: 1, total: 0 })

  useEffect(() => {
    async function perform() {
      try {
        loading()
        setPagination(await api.offers(1, geolocation))
      } catch (e) {
        error('Impossible de contacter le server')
        console.error(e)
      } finally {
        loaded()
      }
    }

    perform()
  }, [ loading, loaded, error, api, geolocation ])

  return (
    <ScrollView>
      <View style={{ backgroundColor: AppTheme.navigationContainer.colors.background, marginBottom: 30, marginTop: 30, paddingLeft: 15, paddingRight: 15 }}>
        <Text h1>Missions</Text>
        <Text>{ pagination.total } missions</Text>
      </View>
      <CardList />
    </ScrollView>
  )
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
    geolocation: state.geolocation,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    error: (message: string) => dispatch(error(message)),
  }
}

const Home = connect(mapStateToProps, mapDispatchToProps)(InternalHome)

export default Home
