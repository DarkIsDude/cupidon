import { StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import RootStackParamList from '../route'

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'NotFound'>
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  link: {
    marginTop: 15,
    paddingVertical: 15,
  },
  linkText: {
    fontSize: 14,
  },
})

export default function NotFound(props: Props) {
  const { navigation } = props

  return (
    <View style={style.container}>
      <Text style={style.title}>Oups ! This page doesn't exist...</Text>
      <TouchableOpacity onPress={() => navigation.replace('Home')} style={style.link}>
        <Text style={style.linkText}>Go back to home !</Text>
      </TouchableOpacity>
    </View>
  )
}
