type RootStackParamList = {
  Home: undefined
  Detail: { id: number }
  Login: undefined
  SignIn: undefined
  NotFound: undefined
}

export default RootStackParamList
