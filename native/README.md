# Cupidon Native

## Command from the project

- yarn start # you can open iOS, Android, or web from here, or run them directly with the commands below.
- yarn android
- yarn ios
- yarn web
- npx pod-install: [@see](https://www.npmjs.com/package/expo-linear-gradient)

## Publish to iOS

`yarn expo build:ios` will build the application with Expo server. When this is done, you can run `yarn expo upload:ios` to upload the app to AppStore.

[Official doc](https://docs.expo.io/distribution/building-standalone-apps/?redirected)

- **Don't forget to setup production env !**
- **Upgrade the version and buildNumber in the app.json**
