import React from 'react'
import { StyleSheet, View } from 'react-native'
import { OfferLight } from 'cupidon-common/state'
import { Text } from 'react-native-elements'

interface Props {
  offer: OfferLight
}

export default function OfferAddress(props: Props) {
  const { offer } = props
  const offerAddress = offer.address()

  return (
    <View>
      { offerAddress.address1 ? <View><Text style={offerAddressStyle.text}>{ offerAddress.address1 }</Text></View> : undefined }
      { offerAddress.address2 ? <View><Text style={offerAddressStyle.text}>{ offerAddress.address2 }</Text></View> : undefined }
      { offerAddress.address3 ? <View><Text style={offerAddressStyle.text}>{ offerAddress.address3 }</Text></View> : undefined }
      { <Text style={offerAddressStyle.text}>{ offerAddress.zipcode } { offerAddress.city }</Text> }
    </View>
  )
}

const offerAddressStyle = StyleSheet.create({
  text: {
    flex: 1,
    flexWrap: 'wrap'
  }
})
