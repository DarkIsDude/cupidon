import React from 'react'
import { StyleSheet, View } from 'react-native'
import { OfferLight } from 'cupidon-common/state'
import { Icon, Text } from 'react-native-elements'

interface Props {
  offer: OfferLight
}

export default function OfferDistance(props: Props) {
  const { offer } = props

  if (offer.distance) {
    return (
      <View style={offerDistanceStyle.item}>
        <Icon name={'transfer-within-a-station'} size={20} iconStyle={offerDistanceStyle.icon} />
        <Text>&Agrave; {Math.round(offer.distance)}km</Text>
      </View>
    )
  }

  return <></>
}

const offerDistanceStyle = StyleSheet.create({
  item: {
    flexDirection: 'row',
    marginBottom: 15,
    fontSize: 15,
  },
  icon: {
    width: 30
  },
})
