import React, { useState } from 'react'
import { Alert as AlertNative } from 'react-native'
import { Alert as AlertState, AlertType } from 'cupidon-common/state'

interface Props {
  alert: AlertState;
}

export default function Alert(props: Props) {
  const [ showed, setShowed ] = useState(false)

  let title = ''
  switch (props.alert.type) {
    case AlertType.Error:
      title = 'Erreur'
      break
    case AlertType.Success:
      title = 'Succèss'
      break
    default:
      title = 'Information'
      break
  }

  if (!showed) {
    AlertNative.alert(
      title,
      props.alert.message,
      [ { text: 'OK' } ],
    )

    setShowed(true)
  }

  return <></>
}
