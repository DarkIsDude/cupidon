import React from 'react'
import { connect } from 'react-redux'
import { State, Alert as AlertState } from 'cupidon-common/state'
import Alert from '../../atoms/alert'

interface Props {
  alerts: AlertState[];
}

function InternalAlerts(props: Props) {
  return (<>
    {props.alerts.map((alert, key) => <Alert key={key} alert={alert} />)}
  </>)
}

const mapStateToProps = (state: State) => {
  return {
    alerts: state.alerts,
  }
}

const Alerts = connect(mapStateToProps)(InternalAlerts)

export default Alerts
