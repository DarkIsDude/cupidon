import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { error, State, API, User } from 'cupidon-common/state'
import Constants from 'expo-constants'
import * as Notifications from 'expo-notifications'
import { Platform } from 'react-native'
import * as Device from 'expo-device'

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
})

interface Props {
  api: API
  user?: User
  error: (message: string) => void
}

function InternalNotification(props: Props) {
  const { api, user, error } = props

  useEffect(() => {
    const registerNotification = async () => {
      if (Constants.isDevice) {
        const { status: existingStatus } = await Notifications.getPermissionsAsync()
        let finalStatus = existingStatus

        if (existingStatus !== 'granted') {
          const { status } = await Notifications.requestPermissionsAsync()
          finalStatus = status
        }

        if (finalStatus !== 'granted') {
          return
        }

        const token = (await Notifications.getExpoPushTokenAsync()).data

        let deviceName
        if (Device.deviceName) deviceName = Device.deviceName

        try {
          api.usernotificationtokens(token, deviceName, Platform.OS, Platform.Version.toString())
        } catch (e) {
          error(e)
        }
      }
    }

    if (user) {
      registerNotification()
    }
  }, [ api, user, error ])

  return <></>
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    error: (message: string) => dispatch(error(message)),
  }
}

const Notification = connect(mapStateToProps, mapDispatchToProps)(InternalNotification)

export default Notification
