import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { geolocated } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import * as Location from 'expo-location'

interface Props {
  geolocated: (lat: number, lon: number) => void
}

function GeolocationInternal(props: Props) {
  const { geolocated } = props

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync()
      if (status !== 'granted') {
        return
      }

      let location = await Location.getCurrentPositionAsync({})
      geolocated(location.coords.latitude, location.coords.longitude)
    })()
  }, [ geolocated ])

  return (
    <></>
  )
}

const mapStateToProps = () => ({})

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    geolocated: (lat: number, lon: number) => dispatch(geolocated(lat, lon)),
  }
}

const Geolocation = connect(mapStateToProps, mapDispatchToProps)(GeolocationInternal)

export default Geolocation
