import { State } from 'cupidon-common/state'
import React from 'react'
import { Overlay, Text } from 'react-native-elements'
import { connect } from 'react-redux'

interface Props {
  loading: boolean;
}

function InternalLoader(props: Props) {
  return <Overlay isVisible={props.loading}>
    <Text>Chargement</Text>
  </Overlay>
}

const mapStateToProps = (state: State) => {
  return {
    loading: state.loading,
  }
}

const Loader = connect(mapStateToProps)(InternalLoader)

export default Loader
