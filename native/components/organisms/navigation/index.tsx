import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack'
import * as React from 'react'
import LinkingConfiguration from './LinkingConfiguration'
import HomeScreen from '../../../screens/Home'
import DetailScreen from '../../../screens/Detail'
import NotFoundScreen from '../../../screens/NotFound'
import { AppTheme } from '../../../app-theme'
import RootStackParamList from '../../../route'
import { Icon } from 'react-native-elements'
import { StyleSheet } from 'react-native'
import LoginScreen from '../../../screens/Login'
import { error, loaded, loading, State, User, API as IAPI, success, logout } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import SignInScreen from '../../../screens/SignIn'

interface Props {
  api: IAPI
  user?: User
  loading: () => void
  loaded: () => void
  logout: () => void
  success: (message: string) => void
  error: (message: string) => void
}

const Stack = createStackNavigator<RootStackParamList>()

const style = StyleSheet.create({
  icon: {
    marginRight: 10,
    color: AppTheme.colors.white
  },
})

const headerOptions = {
  headerRightContainerStyle: {
    marginRight: 15
  },
}

function InternalNavigation(props: Props) {
  const { user, logout } = props

  const userButton = (navigation: StackNavigationProp<RootStackParamList>) => {
    return user
      ? () => <Icon style={style.icon} name="sign-out-alt" type="font-awesome-5" onPress={() => {logout(); success('Déconnecté'); navigation.navigate('Home')}} />
      : () => <Icon style={style.icon} name="user" type="font-awesome-5" onPress={() => navigation.navigate('Login')} />
  }

  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={AppTheme.navigationContainer}>
      <Stack.Navigator initialRouteName="Home" mode={'modal'} screenOptions={{
        headerStyle: {
          backgroundColor: AppTheme.colors.whiteDarker,
          borderBottomWidth: 1
        },
        headerTintColor: AppTheme.colors.blackLight,
      }}>
        <Stack.Screen name="Home" component={HomeScreen} options={({ navigation }) => ({ title: 'Solidarity Now', headerRight: userButton(navigation), ...headerOptions })} />
        <Stack.Screen name="Detail" component={DetailScreen} options={({ navigation }) => ({ title: '', headerRight: userButton(navigation), ...headerOptions })} />
        <Stack.Screen name="Login" component={LoginScreen} options={() => ({ title: 'Se connecter', ...headerOptions })}/>
        <Stack.Screen name="SignIn" component={SignInScreen} options={() => ({ title: 'S\'enregistrer', ...headerOptions })}/>
        <Stack.Screen name="NotFound" component={NotFoundScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    logout: () => dispatch(logout()),
    success: (message: string) => dispatch(success(message)),
    error: (message: string) => dispatch(error(message)),
  }
}

const Navigation = connect(mapStateToProps, mapDispatchToProps)(InternalNavigation)

export default Navigation
