import React, { useEffect, useState } from 'react'
import { StyleSheet, View } from 'react-native'
import { Button, Icon } from 'react-native-elements'
import { connect } from 'react-redux'
import { error, loaded, loading, State, API as IAPI, Pagination, OfferLight, Geolocation } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import { useNavigation } from '@react-navigation/native'
import OffersCard from '../card'
import OffersCardSkeleton from '../card-skeleton'
import { AppTheme } from '../../../app-theme'

const style = StyleSheet.create({
  header: {
    color: AppTheme.colors.white,
  },
  offerContainer: {
    marginLeft: 15,
    marginBottom: 15,
    marginRight: 15
  }
})

interface Props {
  api: IAPI
  geolocation: Geolocation
  loading: () => void
  loaded: () => void
  error: (message: string) => {}
}

function InternalCardList(props: Props) {
  const { loading, loaded, error, api, geolocation } = props
  const navigation = useNavigation()
  const [ pagination, setPagination ] = useState<Pagination<OfferLight>>({ current_page: 1, data: [], last_page: 1, total: 0 })
  const [ isLoaded, setIsLoaded ] = useState<boolean>(false)

  useEffect(() => {
    async function perform() {
      try {
        loading()
        setPagination(await api.offers(pagination.current_page, geolocation))
      } catch (e) {
        error('Impossible de contacter le server')
        console.error(e)
      } finally {
        loaded()
        setIsLoaded(true)
      }
    }

    perform()
  }, [ pagination.current_page, loading, loaded, error, api, geolocation ])

  const changePage = (page: number) => {
    setPagination({ current_page: page, data: [], last_page: pagination.last_page, total: pagination.total })
  }

  return <>
    {isLoaded
      ? <>
        {pagination.data.map((offer: OfferLight) => {
          return (
            <View style={style.offerContainer} key={'offercard_' + offer.id}>
              <OffersCard offer={offer} onPress={() => navigation.navigate('Detail', { id: offer.id })} />
            </View>
          )
        })}
      </> : <>
        {[ 1, 2, 3 ].map((index) => (
          <View style={style.offerContainer} key={`offercard_${index}`}>
            <OffersCardSkeleton />
          </View>
        ))}
      </>
    }
    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignContent: 'space-between', margin: 15 }}>
      {pagination.prev_page_url ? <Button onPress={() => changePage(pagination.current_page - 1)} icon={<Icon name="arrow-circle-left" type="font-awesome-5" />} /> : <View></View>}
      {pagination.next_page_url ? <Button onPress={() => changePage(pagination.current_page + 1)} icon={<Icon name="arrow-circle-right" type="font-awesome-5" />} /> : <View></View>}
    </View>
  </>
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
    geolocation: state.geolocation,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    error: (message: string) => dispatch(error(message)),
  }
}

const CardList = connect(mapStateToProps, mapDispatchToProps)(InternalCardList)

export default CardList
