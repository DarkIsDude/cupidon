import React from 'react'
import { Card } from 'react-native-elements'
import SkeletonContent from 'react-native-skeleton-content'

export default function OffersCardSkeleton() {
  return (
    <Card>
      <SkeletonContent
        isLoading={true}
        layout={[
          { width: '50%', height: 20, marginBottom: 15, marginTop: 5, marginLeft: 'auto', marginRight: 'auto' },
        ]}
      />
      <Card.Divider/>
      <SkeletonContent
        isLoading={true}
        containerStyle={{ flex: 1, width: '100%', marginTop: 2.5 }}
        layout={[
          {
            flexDirection: 'row',
            marginRight: 10,
            children: [
              {
                width: 15,
                height: 15,
                marginBottom: 12.5
              },
              {
                width: '60%',
                height: 15,
                marginBottom: 12.5,
                marginLeft: 15
              },
            ]
          },
          {
            flexDirection: 'row',
            marginRight: 10,
            children: [
              {
                width: 15,
                height: 15,
                marginBottom: 12.5
              },
              {
                width: '60%',
                height: 15,
                marginBottom: 12.5,
                marginLeft: 15
              },
            ]
          },
          {
            flexDirection: 'row',
            marginRight: 10,
            children: [
              {
                width: 15,
                height: 15,
                marginBottom: 12.5
              },
              {
                width: '60%',
                height: 15,
                marginBottom: 12.5,
                marginLeft: 15
              },
            ]
          },
          { width: '100%', height: 40, marginTop: 2.5 },
        ]}
      />
    </Card>
  )
}
