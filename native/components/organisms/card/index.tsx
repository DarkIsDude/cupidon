import React from 'react'
import { GestureResponderEvent, StyleSheet, View } from 'react-native'
import { OfferLight } from 'cupidon-common/state'
import { Button, Card, Icon, Text } from 'react-native-elements'
import OfferDistance from '../../atoms/offer-distance'
import { LinearGradient } from 'expo-linear-gradient'
import OfferAddress from '../../atoms/offer-address'
import { AppTheme } from '../../../app-theme'

interface Props {
  offer: OfferLight
  onPress: (e: GestureResponderEvent) => void
}

export default function OffersCard(props: Props) {
  const { offer, onPress } = props

  return (
    <Card containerStyle={{ marginBottom: 7.5 }}>
      <Card.Title>{offer.name}</Card.Title>
      <Card.Divider/>
      <View style={ offerCardStyle.item }>
        <Icon name={'place'} size={20} style={ offerCardStyle.icon } />
        <OfferAddress offer={ offer } />
      </View>
      <OfferDistance offer={offer} />
      <View style={ offerCardStyle.item }>
        <Icon name={'insert-invitation'} size={20} style={ offerCardStyle.icon } />
        <Text style={ offerCardStyle.text }>{ offer.start.toDateString() } ({ offer.durationToString() })</Text>
      </View>
      <View style={[ offerCardStyle.item, { marginBottom: 15 } ]}>
        <Icon name={'people'} size={20} style={offerCardStyle.icon} />
        <Text style={ offerCardStyle.text }>{ offer.answers_count } participant{ offer.answers_count > 1 ? 's' : '' }</Text>
      </View>
      <View style={{ alignContent: 'flex-end' }}>
        <LinearGradient colors={[ AppTheme.colors.pinkLight, AppTheme.colors.pink ]} style={{ borderRadius: 15 }}>
          <Button
            title="Voir la mission"
            onPress={onPress}
          />
        </LinearGradient>
      </View>
    </Card>
  )
}

const offerCardStyle = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 7.5
  },
  icon: {
    width: 30
  },
  text: {
    flexWrap: 'wrap',
    flex: 1
  }
})
