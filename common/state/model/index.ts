import { Offer, OfferLight } from './offer'

export * from './offer'

export interface API {
  token?: string;
  env?: string;

  login(email: string, password: string) : Promise<API>;
  signup(email: string, password: string, prefix: string, firstname: string, lastname: string, phone1: string) : Promise<API>;
  me() : Promise<User>;
  offers(page: number, geolocation?: Geolocation) : Promise<Pagination<OfferLight>>;
  offer(id: number, geolocation?: Geolocation) : Promise<Offer>;
  attach(offer: OfferLight) : Promise<void>;
  detach(offer: OfferLight) : Promise<void>;
  usernotificationtokens(token: string, name?: string, os?: string, version?: string) : Promise<void>;
}

export interface State {
    user?: User;
    api: API;
    loading: boolean;
    alerts: Alert[];
    geolocation: Geolocation;
}

export interface Geolocation {
  lat?: number;
  lon?: number;
}

export enum AlertType {
  Error,
  Success,
  Info,
}

export interface Alert {
  message: string;
  type: AlertType;
}

export interface Image {
  id: number;
  url: string;
}

export interface User {
  id: number;
  prefix: string;
  firstname: string;
  lastname: string;
  email: string;
}

export interface Establishment {
  id: number;
  name: string;
  address1?: string;
  address2?: string;
  address3?: string;
  zipcode?: string;
  city?: string;
  phone1?: string;
  phone2?: string;
  lat?: string,
  lon?: string,
  image?: Image;
}

export interface Pagination<T> {
  next_page_url?: string;
  prev_page_url?: string;
  last_page: number;
  current_page: number;
  data: T[];
  total: number;
}
