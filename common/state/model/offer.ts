import { Establishment, Image, User } from '.'

export interface Address {
  address1?: string;
  address2?: string;
  address3?: string;
  zipcode?: string;
  city?: string;
}

export class OfferLight {
  id: number = 0;
  name: string = '';
  start: Date = new Date();
  end: Date = new Date();
  address1?: string;
  address2?: string;
  address3?: string;
  zipcode?: string;
  city?: string;
  country_id?: string;
  auth_user_applied?:	boolean;
  establishment: Establishment = { id: 0, name: '' };
  user: User = { id: 0, prefix: '', firstname: '', lastname: '', email: '' };
  answers_count: number = 0;
  max: number = 0;
  distance?: number;

  public constructor(offer: OfferLight) {
    this.id = offer.id
    this.name = offer.name
    this.start = this._parseDate(offer.start)
    this.end = this._parseDate(offer.end)
    this.address1 = offer.address1
    this.address2 = offer.address2
    this.address3 = offer.address3
    this.zipcode = offer.zipcode
    this.city = offer.city
    this.country_id = offer.country_id
    this.auth_user_applied = offer.auth_user_applied
    this.establishment = offer.establishment
    this.user = offer.user
    this.answers_count = offer.answers_count
    this.max = offer.max
    this.distance = offer.distance
  }

  private _parseDate = (dateString: string|Date): Date => {
    if (dateString instanceof Date) {
      return dateString
    }

    var dateParser = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/
    var match = dateString.match(dateParser)

    if (!match || match.length < 6)
      new Date('force invalid date')

    const date = new Date()
    date.setFullYear(parseInt(match ? match[1] : ''), parseInt(match ? match[2] : '') - 1, parseInt(match ? match[3] : ''))
    date.setHours(parseInt(match ? match[4] : ''), parseInt(match ? match[5] : ''), parseInt(match ? match[6] : ''))

    return date
  }

  startTime(): string{
    return `${this.start.getHours() < 10 ? '0' : ''}${this.start.getHours()}:${this.start.getMinutes() < 10 ? '0' : ''}${this.start.getMinutes()}`
  }

  address(): Address {
    if ([this.address1, this.address2, this.address3, this.zipcode, this.city].join('').trim().length > 0) {
      return {
        address1: this.address1,
        address2: this.address2,
        address3: this.address3,
        zipcode: this.zipcode,
        city: this.city
      }
    } else {
      return {
        address1: this.establishment.address1,
        address2: this.establishment.address2,
        address3: this.establishment.address3,
        zipcode: this.establishment.zipcode,
        city: this.establishment.city
      }
    }
  }

  addressToString(): string {
    const fullAddress = this.address()
    let address = ''
    address += fullAddress.address1 ? ` ${fullAddress.address1}` : ''
    address += fullAddress.address2 ? ` ${fullAddress.address2}` : ''
    address += fullAddress.address3 ? ` ${fullAddress.address3}` : ''
    address += fullAddress.zipcode ? ` ${fullAddress.zipcode}` : ''
    address += fullAddress.city ? ` ${fullAddress.city}` : ''
    return address.trim()
  }

  durationToString() : string {
    let diff = (this.end.getTime() - this.start.getTime()) / 1000

    const days = Math.floor(diff / 86400)
    diff = diff - 86400 * days
    const hours = Math.floor(diff / 3600)
    diff = diff - 3600 * hours
    const minutes = Math.round(diff / 60)

    let duration = ''

    if (days) {
      duration += days + ' jour' + (days > 1 ? 's' : '')
    }

    if (hours) {
      duration += (duration ? ' et ' : '') + hours + ' heure' + (hours > 1 ? 's' : '')
    }

    if (minutes) {
      duration += (duration ? ' et ' : '') + minutes + ' minute' + (minutes > 1 ? 's' : '')
    }

    return duration
  }

  participation() : string {
    return this.answers_count + (this.max > 0 ? ' / ' + this.max : '')
  }

  isExpired() {
    return this.end < new Date();
  }
}

export class Offer extends OfferLight{
  description: string = '';
  images: Image[] = [];
  auth_user_can_apply: boolean = false;

  public constructor(offer: Offer) {
    super(offer)

    this.description = offer.description
    this.images = offer.images
    this.auth_user_can_apply = offer.auth_user_can_apply
  }
}
