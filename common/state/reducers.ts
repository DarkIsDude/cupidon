import { LOGIN, LOGOUT, LOADING, LOADED, ERROR, INFO, SUCCESS, GEOLOCATED, INIT } from './actions'
import { AlertType, State } from './model'
import { API } from '../api'

const initialState: State = {
  api: new API(),
  loading: false,
  alerts: [],
  geolocation: {},
}

export function AppState(state: State = initialState, action?: any) {
  switch (action.type) {
    case INIT:
      return Object.assign({}, state, {
        api: new API(action.env),
        loading: false,
        alerts: [],
        geolocation: {},
      })
    case LOGIN:
      return Object.assign({}, state, {
        user: action.user,
        api: action.api,
      })
    case LOGOUT:
      return Object.assign({}, state, {
        user: null,
        api: new API(state.api.env),
      })
    case LOADING:
      return Object.assign({}, state, {
        loading: true,
      })
    case LOADED:
      return Object.assign({}, state, {
        loading: false,
      })
    case ERROR:
      return Object.assign({}, state, {
        alerts: [...state.alerts, { message: action.message, type: AlertType.Error }],
      })
    case INFO:
      return Object.assign({}, state, {
        alerts: [...state.alerts, { message: action.message, type: AlertType.Info }],
      })
    case SUCCESS:
      return Object.assign({}, state, {
        alerts: [...state.alerts, { message: action.message, type: AlertType.Success }],
      })
    case GEOLOCATED:
      return Object.assign({}, state, {
        geolocation: {
          lat: action.lat,
          lon: action.lon,
        }
      })
    default:
      return state
  }
}
