import { User, API } from './model'

export const INIT = 'INIT'

export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const LOADING = 'LOADING'
export const LOADED = 'LOADED'
export const ERROR = 'ERROR'
export const INFO = 'INFO'
export const SUCCESS = 'SUCCESS'
export const GEOLOCATED = 'GEOLOCATED'

export function login(user: User, api: API) {
  return {
    type: LOGIN,
    user,
    api,
  }
}

export function logout() {
  return {
    type: LOGOUT,
  }
}

export function loading() {
  return {
    type: LOADING,
  }
}

export function loaded() {
  return {
    type: LOADED,
  }
}

export function error(message: string) {
  return {
    type: ERROR,
    message,
  }
}

export function info(message: string) {
  return {
    type: INFO,
    message,
  }
}

export function success(message: string) {
  return {
    type: SUCCESS,
    message,
  }
}

export function geolocated(lat: number, lon: number) {
  return {
    type: GEOLOCATED,
    lat,
    lon,
  }
}
