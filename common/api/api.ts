import axios, { AxiosInstance } from 'axios'
import { User, OfferLight, Offer, API as IAPI, Geolocation, Pagination } from '../state/model'

export const COOKIES_NAME = 'cupidon-token'
export const ONE_WEEK_IN_SECONDS = 604800

export class API implements IAPI {
  env?: string = "";
  axios: AxiosInstance;
  token?: string;

  constructor(env?: string, token?: string) {
    const baseURL = env === 'production' ? 'https://www.solidaritynow.fr/api/' : 'https://cupidon.robinlaurens.fr/api/'

    this.env = env
    this.token = token
    this.axios = axios.create({
      baseURL,
      headers: {'Authorization': `Bearer ${token}`}
    })
  }

  async login(email: string, password: string) : Promise<IAPI> {
    const bodyFormData = new FormData()
    bodyFormData.append('email', email)
    bodyFormData.append('password', password)
    const response = await this.axios.post('auth/login', bodyFormData)

    return new API(this.env, response.data.access_token)
  }

  async signup(email: string, password: string, prefix: string, firstname: string, lastname: string, phone1: string) : Promise<IAPI> {
    const bodyFormData = new FormData()
    bodyFormData.append('email', email)
    bodyFormData.append('password', password)
    bodyFormData.append('prefix', prefix)
    bodyFormData.append('firstname', firstname)
    bodyFormData.append('lastname', lastname)
    bodyFormData.append('phone1', phone1)
    const response = await this.axios.post('auth/signup', bodyFormData)

    return new API(this.env, response.data.access_token)
  }

  async me() : Promise<User> {
    const response = await this.axios.get('user')
    return response.data
  }

  async offers(page: number, geolocation?: Geolocation) : Promise<Pagination<OfferLight>> {
    // We need to do that because we have a network error with native on cupidon.robinlaurens.fr api if page = 1
    const customPage = page === 1 ? undefined : page;
    const response = await this.axios.get('offers', { params: { page: customPage, lat: geolocation?.lat, lon: geolocation?.lon } })

    const pagination: Pagination<OfferLight> = response.data
    pagination.data = pagination.data.map((offer: OfferLight) => new OfferLight(offer))

    return pagination
  }

  async offer(id: number, geolocation?: Geolocation) : Promise<Offer> {
    const response = await this.axios.get(`offers/${id}`, { params: { lat: geolocation?.lat, lon: geolocation?.lon } })
    return new Offer(response.data)
  }

  async attach(offer: OfferLight) : Promise<void> {
    await this.axios.post(`user/offers/${offer.id}/attach`)
  }

  async detach(offer: OfferLight) : Promise<void> {
    await this.axios.post(`user/offers/${offer.id}/detach`)
  }

  async usernotificationtokens(token: string, name?: string, os?: string, version?: string) : Promise<void> {
    const bodyFormData = new FormData()
    bodyFormData.append('token', token)
    if (name) bodyFormData.append('name', name)
    if (os) bodyFormData.append('os', os)
    if (version) bodyFormData.append('version', version)

    await this.axios.post(`user/usernotificationtokens`, bodyFormData)
  }
}
