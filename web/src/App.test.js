import React from 'react'
import { render } from '@testing-library/react'
import App from './App'
import { Provider } from 'react-redux'
import { AppState } from 'cupidon-common/state'
import { createStore } from 'redux'
import { ThemeProvider } from 'styled-components'
import { createMuiTheme, ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'

test('renders learn react link', () => {
  const store = createStore(AppState)
  const customTheme = createMuiTheme({})

  const { getByTestId } = render(
    <Provider store={store}>
      <MuiThemeProvider theme={customTheme}>
        <ThemeProvider theme={customTheme}>
          <App/>
        </ThemeProvider>
      </MuiThemeProvider>
    </Provider>
  )
  const divElement = getByTestId('app-main')
  expect(divElement).toBeInTheDocument()
})
