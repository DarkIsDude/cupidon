import React from 'react'
import { connect } from 'react-redux'
import { State } from 'cupidon-common/state'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import Loader from './components/atoms/loader'
import AutomaticLoginRoute from './components/molecules/automatic-login-route'
import Alerts from './components/molecules/alerts'
import PWABanner from './components/organisms/pwa-banner'
import Geolocation from './components/organisms/geolocation'
import styled from 'styled-components'

import Index from './pages/index'
import Signin from './pages/signin'
import Login from './pages/login'
import Detail from './pages/detail'
import CGU from './pages/cgu'

import './App.css'

const Container = styled.div`
  height: 100%;
  max-height: 100%;
`

interface Props {
  loading: boolean;
}

function InternalApp(props: Props) {
  return <Container data-testid="app-main">
    <PWABanner />
    <Loader open={props.loading} />
    <Geolocation />
    <Alerts />
    <Router>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/offer/:id" component={Detail} />
        <Route path="/cgu" component={CGU} />
        <AutomaticLoginRoute path="/signin" component={Signin} />
        <AutomaticLoginRoute path="/" component={Index} />
      </Switch>
    </Router>
  </Container>
}

const mapStateToProps = (state: State) => {
  return {
    loading: state.loading,
  }
}

const App = connect(mapStateToProps)(InternalApp)

export default App
