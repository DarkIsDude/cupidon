import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { CssBaseline } from '@material-ui/core'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { ThemeProvider } from 'styled-components'
import { createMuiTheme, ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'

import { AppState, INIT } from 'cupidon-common/state'

const store = createStore(AppState)
store.dispatch({ type: INIT, env: process.env.REACT_APP_ENV_NAME })

const customTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#81d4f2',
      main: '#00AFF5',
      dark: '#009cf6',
      contrastText: '#fff',
    },
    secondary: {
      light: '#76d58a',
      main: '#00d657',
      dark: '#00d600',
      contrastText: '#000',
    },
    background: {
      default: '#fff',
    }
  },
})

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <MuiThemeProvider theme={customTheme}>
        <ThemeProvider theme={customTheme}>
          <CssBaseline />
          <App />
        </ThemeProvider>
      </MuiThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)

serviceWorker.unregister()
