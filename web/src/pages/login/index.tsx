import React, { ReactElement, useEffect } from 'react'
import { TextField, CardActions, Divider } from '@material-ui/core'
import Button from '../../components/atoms/button'
import Menu from '../../components/organisms/menu'
import { connect } from 'react-redux'
import { login, State, User, loading, loaded, error, success, API as IAPI } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import { API, COOKIES_NAME, ONE_WEEK_IN_SECONDS } from 'cupidon-common/api'
import { Link, Redirect } from 'react-router-dom'
import { CardContainer, CardContentColumn, Container, ExtentedCard } from '../../components/templates/single-card'
import { useCookies } from 'react-cookie'

interface Props {
  user?: User;
  login(user: User, api: IAPI): void;
  api: IAPI;
  loaded: () => void;
  loading: () => void;
  isLoading: boolean;
  error: (message: string) => {};
  success: (message: string) => {};
}

function InternalLogin(props: Props): ReactElement {
  const [email, setEmail] = React.useState<string>('')
  const [password, setPassword] = React.useState<string>('')
  const [error, setError] = React.useState<boolean>(false)
  const [cookies, setCookie, removeCookie] = useCookies([COOKIES_NAME])

  useEffect(() => {
    async function triggerLogin() {
      try {
        props.loading()
        let user: User
        const token = cookies[COOKIES_NAME]
        let api: IAPI

        if (token) {
          api = new API(props.api.env, token)
          user = await api.me()
          props.login(user, api)
        } else {
          api = await props.api.login(email, password)
          setCookie('cupidon-token', api.token, { path: '/', maxAge: ONE_WEEK_IN_SECONDS })
          props.success('Connecté')
        }
      } catch (error) {
        props.error('Erreur lors de votre connexion')
        console.error(error)
        removeCookie(COOKIES_NAME)
        setError(true)
      } finally {
        props.loaded()
      }
    }

    if ((props.isLoading || cookies[COOKIES_NAME]) && !props.user) {
      triggerLogin()
    }
  }, [cookies, props, removeCookie, email, password, setCookie])

  if (props.user) {
    return <Redirect to='/' />
  }

  async function login(): Promise<void> {
    if (!email || !password) {
      return setError(true)
    }

    props.loading()
  }

  return (
    <Container>
      <Menu />
      <CardContainer>
        <ExtentedCard variant="outlined">
          <CardContentColumn>
            <TextField fullWidth error={error} required id="email" label="Email" type="email" value={email} onChange={e => setEmail(e.target.value)} />
            <TextField fullWidth error={error} required id="password" label="Mot de passe" type="password" value={password} onChange={e => setPassword(e.target.value)} />
          </CardContentColumn>
          <CardActions>
            <Button color="primary" onClick={async () => await login()} disabled={props.isLoading}>
              Se connecter
            </Button>
            <Divider orientation="vertical" flexItem />
            <Button component={Link} to="/signin" disabled={props.isLoading}>
              S'inscrire
            </Button>
          </CardActions>
        </ExtentedCard>
      </CardContainer>
    </Container>
  )
}

const mapStateToProps = (state: State) => {
  return {
    user: state.user,
    api: state.api,
    isLoading: state.loading,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    login: (user: User, api: IAPI) => dispatch(login(user, api)),
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    error: (message: string) => dispatch(error(message)),
    success: (message: string) => dispatch(success(message)),
  }
}

const Login = connect(mapStateToProps, mapDispatchToProps)(InternalLogin)

export default Login
