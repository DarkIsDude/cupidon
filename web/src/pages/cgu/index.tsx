import React from 'react'
import { Container } from '../../components/templates/single-card'
import Menu from '../../components/organisms/menu'
import { Container as MUIContainer } from '@material-ui/core'
import Footer from '../../components/organisms/footer'

function CGU() {
  return <Container>
    <Menu />
    <MUIContainer fixed>
      <h1>CGU de l’application mobile « Solidaritynow »</h1>

      <h2>ARTICLE 1 – PERIMETRE ET ACCEPTATION DES CGU</h2>
      <p>Les conditions générales d’utilisation (« CGU ») ont pour objet de déterminer les règles d’utilisation de l’application mobile « SolidarityNow » (l’« Application ») éditée par La Mêlée contre l’Isolement par l’ACTR (« l’éditeur »).Les CGU s’appliquent aux relations entre l’utilisateur, usager ou visiteur de l’application SolidarityNow, et l’éditeur de l’application.Elles déterminent les droits des usagers de SolidarityNow et de l’éditeur de l’application, ainsi que les droits éventuellement détenus par des tiers en matière de propriété intellectuelle ou de propriété des données.Elles déterminent également les obligations de l’éditeur, et de l’utilisateur, usager ou visiteur de l’Application.L’utilisateur, usager ou visiteur de l’application «SolidarityNow» est tenu de se conformer au CGU objets des présentes sans conditions ni réserves.L’accès à l’application implique l’acceptation préalable et sans réserve des présentes CGU par l’utilisateur, l’usager ou le visiteur de l’application SolidarityNow, concernant ses droits et obligations.L’accès à l’application doit se faire dans le strict cadre posé par les CGU.L’éditeur de l’application SolidarityNow s’engage à respecter scrupuleusement les dispositions des présentes CGU, notamment en matière de protection et traitement des données personnelles des utilisateurs, usagers ou visiteurs de l’application «SolidarityNow».Les CGU s’imposent aux parties, et il y sera référé en cas de litigeElles peuvent être modifiées unilatéralement par l’éditeur ; l’utilisateur, l’usager ou le visiteur de l’application SolidarityNow  sera avisé de toute évolution des CGU, et aura alors le choix de demeurer usager de l’application, ou de cesser de l’utiliser compte tenu des modifications apportées aux CGU.</p>

      <h2>ARTICLE 2 – LEXIQUE</h2>
      <p>Les mots employés dans les présentes CGU auront la définition suivante :</p>
      <p>Le terme « L’Application » défini l’application logicielle «SolidarityNow» éditée par La Mêlée contre l’Isolement par l’ACTR Ce terme désigne aussi tous les contenus de l’Application SolidarityNow.</p>
      <p>Le terme « Contenu » désigne de façon non exhaustive la structure de l’Application SolidarityNow, les chartes graphiques, marques, logos, sigles, dénominations sociales, notamment.</p>
      <p>Le terme « Services » désignent les différentes fonctionnalités et services proposés par l’Application.</p>
      <p>Le « Site » désigne le site internet de l’application SolidarityNow dont l’adresse est la suivante : https://www.solidaritynow.fr </p>
      <p>L’ « Utilisateur, usager ou visiteur » désigne la personne physique majeure ou mineure ayant préalablement obtenu l’autorisation de ses représentant légaux et ayant téléchargé l’Application dans le cadre d’un usage strictement personnel, à l’exclusion de tout usage commercial et lucratif, direct ou indirect.</p>
      <p>Une « Mission » désigne l’ensemble des actions qui sont proposés et qu’un bénévole peut réaliser. Lors de chaque mission, il doit se conforter aux principes moraux de l’Association la Mêlée contre l’Isolement et avoir une posture qui ne nuira pas à l’association. Enfin, lorsqu’il participe à ses missions, le bénévole accepte de ne pas se retourner contre l’association et engage sa responsabilité.</p>

      <h2>ARTICLE 3 – OBJET DE L’APPLICATION SolidarityNow</h2>
      <p>L’application SolidarityNow a pour objet :</p>
      <ul>
        <li>D’aider les utilisateurs a s’engager dans le domaine humanitaire et social. Pour cela elle met en lien des associations, institutions ou particuliers qui expriment un besoin et des bénévoles qui peuvent répondre à ce besoin. </li>
        <li>L’application propose à ces différentes fins notamment :</li>
        <li>Les missions futures </li>
        <li>Les missions passées </li>
        <li>Les missions actuelles </li>
        <li>La mise en relation entre les demandeurs d’aide et les bénévoles</li>
        <li>Des notifications putsch </li>
        <li>Des publicités</li>
      </ul>
      <p>Cette liste est susceptible d’être modifiée par l’Editeur de l’Application à tout moment conformément aux dispositions de l’article 1 des présentes CGU.</p>

      <h2>ARTICLE 4 – MATERIEL NECESSAIRE ET COÛT D’ACCES A L’APPLICATION SolidarityNow</h2>
      <p>Pour accéder à l’Application SolidarityNow, l’utilisateur, usager ou visiteur de l’application doit être muni d’un téléphone portable ou terminal mobile compatible et d’un accès au réseau Internet.L’Application SolidarityNow peut être téléchargée via l’« Apple Store » et le « Google Play Store » sur un terminal Apple iOS et Android.La version du logiciel de l’Application peut faire l’objet de mises à jour régulières dans le but de d’optimiser ses fonctionnalités, de proposer d’avantages de services à l’utilisateur, usager ou visiteur, ou de se conformer à l’évolution de la législation.Pour continuer à bénéficier de toutes les fonctionnalités de l’Application SolidarityNow, l’utilisateur, usager ou visiteur devra procéder aux mises à jour régulièrement.Le téléchargement et l’utilisation de l’Application SolidarityNow  sont gratuits pour l’utilisateur, usager ou visiteur.Cette gratuité s’entend hors coût de l’abonnement téléphonique et hors coût de connexion et de la navigation internet nécessairement à charge de l’utilisateur, usager ou visiteur de l’application SolidarityNow.</p>

      <h2>ARTICLE 5 – ACCESSIBILITE DE L’APPLICATION</h2>
      <p>L’Application SolidarityNow  est disponible 24h/24, 7 jours/7, 365 jours par an.L’Editeur s’engage à garantir cette accessibilité, mais ne peut être tenu responsable de la suspension de l’accès dans les cas suivants :</p>
      <p>Force majeure ou évènement fortuit indépendant de la volonté de l’Editeur</p>
      <p>Panne internet ou défaut d’accessibilité au réseau</p>
      <p>Opération de maintenance nécessaire</p>

      <h2>ARTICLE 6 – CARACTÉRISTIQUES DE LA LICENCE D’UTILISATION ACCORDÉE A L’USAGER DE L’APPLICATION SolidarityNow</h2>
      <p>La licence d’utilisation accordée à l’utilisateur, usager ou visiteur de l’application SolidarityNow est gratuite.Il s’agit d’un droit strictement personnel de l’utilisateur, usager ou visiteur de l’application SolidarityNowLa licence d’utilisation de l’Application SolidarityNow n’est ni cessible, ni transférable, à titre gratuit ou à titre onéreux.La licence d’utilisation octroyée est un droit d’usage précaire et par définition révocable.Son maintien au bénéfice de l’utilisateur, usager ou visiteur de l’application SolidarityNow  est subordonné au strict respect des CGU par l’utilisateur, usager ou visiteur de l’application SolidarityNow.Il est précisé que l’octroi d’une licence d’utilisation ne permet en aucun cas à l’utilisateur, usager ou visiteur de l’application SolidarityNow  un accès aux codes source ou aux composants logiciel de l’Application.</p>

      <h2>ARTICLE 7 – ENGAGEMENTS DE L’UTILISATEUR DE SolidarityNow</h2>
      <p>L’utilisateur, usager ou visiteur de l’application SolidarityNow s’engage à utiliser généralement l’application conformément aux CGU, et plus particulièrement, de façon non exhaustive :</p>
      <p>A faire un usage personnel de l’Application, à l’exclusion de tous autres usages,</p>
      <p>A effectuer les mises à jour nécessaires au bon fonctionnement de l’Application SolidarityNow,</p>
      <p>A ne pas reproduire, adapter, distribuer, copier, modifier l’Application SolidarityNow et/ou l’un de ses contenus, pour quelque cause et par quelque moyen que ce soit, à titre gratuit ou onéreux,</p>
      <p>A ne pas pirater l’Application et/ou l’un de ses contenus et/ou effectuer d’opérations susceptibles d’attenter à l’intégrité et/ou à la sécurité de l’application et/ou de l’un de ses contenus et des données personnelles des utilisateurs,</p>
      <p>A ne pas s’introduire au sein des bases de données de l’Application SolidarityNow, les extraire, ou en faire quelque usage que ce soit,</p>
      <p>A alerter l’Editeur de la détection d’acte de malveillance informatique,</p>
      <p>A s’abstenir de tout comportement malveillant, à l’égard des autres utilisateurs et de l’Editeur,</p>
      <p>A ne pas distribuer pour quelque cause que ce soit et de quelque façon que ce soit l’Application et/ou l’un de ses contenus à des tiers, à titre gratuit ou onéreux.</p>

      <h2>ARTICLE 8 – RÉSILIATION DE L’ACCORD DE LA LICENCE D’UTILISATION</h2>
      <p>En l’absence de respect des conditions d’utilisation, la licence d’utilisation sera immédiatement suspendue puis retirée à l’issue d’un délai de 24h suivant l’information qui en sera faite à l’utilisateur, usager ou visiteur par mail.L’utilisation de l’Application devra immédiatement cesser.</p>

      <h2>ARTICLE 9 – LIMITATIONS DE GARANTIE</h2>
      <p>L’utilisation de l’Application SolidarityNow  se fait sous la responsabilité exclusive de l’utilisateur, usager ou visiteur de l’Application.L’utilisateur, usager ou visiteur de l’Application SolidarityNow prendra toutes les mesures de nature à protéger ses propres données et logiciels présents sur ses équipements informatiques et téléphoniques contre les atteintes de toute nature, virus et piratage.La Mêlée contre l’isolement par l’ACTR ne pourra être tenue responsable dans l’hypothèse d’une mauvaise transmission ou réception des données à l’utilisateur, d’une corruption de celles-ci, d’une mauvaise utilisation des équipements utilisés pour accéder à l’Application SolidarityNow et de tout dommage, de quelque nature que ce soit, causé à l’utilisateur, usager ou visiteur de l’Application ainsi qu’aux équipements utilisés pour accéder à l’Application SolidarityNow et aux données qui y sont contenues, ou encore des répercussions personnelles ou professionnelles qui pourraient être corrélées.</p>

      <h2>ARTICLE 10 – PROPRIÉTÉ DE L’APPLICATION</h2>
      <p>La Mêlée contre l’Isolement par l’ACTR, éditeur de l’application, est propriétaire exclusif de tous les droits de propriété littéraire, artistique et industrielle relatifs à l’Application SolidarityNow et à ses contenus, comprenant non limitativement : brevets déposés, noms commerciaux, enseignes, sigles, logos, chartes graphiques, contenus, commentaires, images, photographies, visuels, textes, animations, vidéos, dessins, slogans publicitaires.Toute reproduction totale ou partielle de ces éléments propriété exclusive de l’éditeur, à des fins mercantiles ou non, pour quelque cause que ce soit et par quelque procédé que ce soit, sans autorisation expresse de la Mêlée contre l’Isolement par l’ACTR est strictement interdite.L’octroi d’une licence d’utilisation à l’utilisateur, usager ou visiteur de l’application SolidarityNow tel que prévu à l’article 6 des présentes CGU ne s’analyse en aucun cas en une cession totale ou partielle de droit au sens du code de la propriété intellectuelle.Elle ne confrère à l’utilisateur, usager ou visiteur de l’application SolidarityNow  aucun droit de propriété intellectuelle sur l’Application SolidarityNow et l’ensemble de ses contenus, demeurant la propriété exclusive de l’Editeur.Toute reproduction, adaptation, distribution des contenus est strictement interdite.L’appropriation de l’Application et de ses contenus pour quelque cause que ce soit et par quelque procédé que ce soit, à titre gratuit ou onéreux, constituerait un acte de contrefaçon susceptible de poursuites, indépendamment du retrait de la licence d’utilisation.</p>

      <h2>ARTICLE 11 – PATERNITÉ ET SINCÉRITÉ DES DONNÉES PRÉSENTÉES SUR L’APPLICATION SolidarityNow </h2>
      <p>La Mêlée contre l’Isolement par l’ACTR ne garantit pas l’exactitude et l’exhaustivité des données figurant sur l’Application SolidarityNow et ses bases de données. Ces dernières étant inscrites par les demandeurs d’aide qu’il s’agisse d’associations partenaires, d’institutions ou de particuliers. Il est en effet possible qu’une mission soit présente sur l’Application, mais que toutes ses caractéristiques ne soient pas renseignées, ou que certaines informations soient erronées, dans la mesure où elles émanent de contributeurs citoyens.La Mêlée contre l’Isolement par l’ACTR  ne saurait être tenue responsable de quelque manière que ce soit de tout ce qui pourrait se passer au cours d’une mission ou sur le chemin qu’empreinte un utilisateur pour la réaliser.  </p>

      <h2>ARTICLE 12 – PROTECTION DES DONNÉES PERSONNELLES DE L’UTILISATEUR, USAGER OU VISITEUR DE L’APPLICATION SolidarityNow</h2>
      <p>La Mêlée contre l’Isolement par l’ACTR, Editeur de l’Application SolidarityNow collecte les données personnelles de l’utilisateur, usager ou visiteur uniquement dans le but de réaliser les prestations proposées via l’Application SolidarityNow.La Mêlée contre l’Isolement par l’ACTR ne collecte que les données dont le traitement est rendu strictement nécessaire à la bonne exécution des prestations proposées via l’Application SolidarityNow, ainsi que les données de navigation recueillies par l’usage des cookies.Elle se conforme ainsi au principe de minimisation dans le recueil des données, qui consiste, en amont de la collecte, pour le prestataire, à s’interroger sur l’absolue nécessité de collecter une donnée ; en d’autres termes, seront uniquement collectées les données dont le traitement est rendu indispensable au bon fonctionnement de l’Application.L’utilisateur, usager ou visiteur de l’Application SolidarityNow  a par ailleurs le choix de transmettre ou non un certain nombre de données à l’Editeur via l’Application SolidarityNow.Cependant, si les données communiquées sont insuffisantes, l’utilisateur, usager ou visiteur de l’Application SolidarityNow ne pourra bénéficier de l’ensemble des fonctionnalités de l’Application SolidarityNow, l’accès à certains services pouvant être restreint en l’absence d’identification suffisante de l’utilisateur, usager ou visiteur.L’utilisateur, usager ou visiteur pourra toutefois bénéficier des fonctionnalités de base, et La Mêlée contre l’Isolement par l’ACTR ne pourra être tenu responsable de cette accessibilité minimale.Les données personnelles communiquées via l’Application SolidarityNow seront collectées et traitées pendant le temps d’utilisation de l’Application.Les données collectées et traitées par La Mêlée contre l’Isolement par l’ACTR peuvent être transmises à l’industrie pharmaceutique, à des associations ou des institutions afin de fournir à l’utilisateur, usager ou visiteur de l’Application SolidarityNow des missions, des services ou des avantages. La Mêlée contre l’Isolement par l’ACTR traite les données personnelles en son sein, et les transmets aux industriels, institutions et associations sous forme de statistiques anonymisées.La Mêlée contre l’Isolement par l’ACTR  ne revend en aucun cas les données nominatives de l’utilisateur, usager ou visiteur de l’Application GREEN CODE.L’utilisateur, usager ou visiteur de l’Application SolidarityNow peut à tout moment demander la rectification des informations personnelles le concernant.Il dispose également du droit à l’effacement de ses données personnelles.Toutefois, ce droit à l’effacement ne saurait être mis en mouvement avant l’expiration des délais de prescription et de forclusion en matière contractuelle et fiscale.La Mêlée contre l’Isolement par l’ACTR  met tout en œuvre pour que la collecte, le traitement et la conservation des données soient opérés dans des conditions de sécurité optimales.La Mêlée contre l’Isolement par l’ACTR  utilise des cookies de navigation sur son site internet.Ils garantissent une navigation plus rapide et permettent à l’Editeur de l’Application d’obtenir des statistiques de fréquentation.</p>

      <h2>ARTICLE 13 – DROIT APPLICABLE</h2>
      <p>Les relations entre l’utilisateur, usager ou visiteur de l’Application SolidarityNow et La Mêlée contre l’Isolement par l’ACTR  sont soumises à la législation en vigueur sur le territoire français.En cas de litige, le droit commun sera appliqué concernant la détermination du Tribunal territorialement et matériellement compétent.</p>

      <hr />
      <p>A TOULOUSE LE 16/02/2021</p>
    </MUIContainer>
    <Footer />
  </Container>
}

export default CGU
