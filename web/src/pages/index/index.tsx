import React, { useCallback, useEffect, useState } from 'react'
import Menu from '../../components/organisms/menu'
import BackgroundImage from './background.svg'
import CardAnnonce from '../../components/molecules/card-annonce'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import styled from 'styled-components'
import { State, OfferLight, loading, loaded, error, API, Geolocation } from 'cupidon-common/state'
import { connect } from 'react-redux'
import AnnonceList from '../../components/organisms/annonce-list'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowDown } from '@fortawesome/free-solid-svg-icons'
import { Divider, Hidden, Typography } from '@material-ui/core'
import { Dispatch } from 'redux'
import Footer from '../../components/organisms/footer'

const IndexContainer = styled.div`
  min-height: 100%;
  height: 100%;
`

const Background = styled.div`
  background-image: url(${BackgroundImage});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`

const OfferContainer = styled.div`
  display: flex;
  align-items: start;
  flex: 1;
  justify-content: center;
  padding: 3rem;
  color: ${({ theme }) => theme.palette.background.default};
`

const BodyContainer = styled(Container)`
  margin-top: 2rem;
  margin-bottom: 2rem;
`

const More = styled.div`
  padding: 1rem;
  margin: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background-color: ${({ theme }) => theme.palette.background.default};
  border-top: 1px solid ${({ theme }) => theme.palette.primary.main};
`

const BounceIcon = styled(FontAwesomeIcon)`
  @keyframes bounce {
    0%, 20%, 50%, 80%, 100% {transform: translateY(0);}
    40% {transform: translateY(-4rem);}
    60% {transform: translateY(-1rem);}
  }

  font-size: 1.5rem;
  color: ${({ theme }) => theme.palette.secondary.main};
  animation: bounce 1s infinite;
`

const Spacer = styled(Divider)`
  margin: 4rem;

  @media (max-height: 50rem) {
    display: none;
  }
`

const OnlyEnoughHeight = styled.div`
  @media (max-height: 40rem) {
    display: none;
  }
`

const Title = styled(Typography)`
  @media (max-height: 28rem) {
    display: none;
  }

  ${({ theme }) => theme.breakpoints.down('sm')} {
    font-size: 2rem;
  }
`

const SubTitle = styled(Typography)`
  @media (max-height: 30rem) {
    display: none;
  }

  ${({ theme }) => theme.breakpoints.down('sm')} {
    font-size: 1rem;
  }
`

interface Props {
  api: API;
  geolocation: Geolocation;
  loading: () => void;
  loaded: () => void;
  error: (message: string) => {};
}

function InternalIndex(props: Props) {
  const [offers, setOffers] = useState<OfferLight[]>([])

  const loadOffers = useCallback(() => {
    async function perform() {
      try {
        props.loading()
        const pagination = await props.api.offers(1, props.geolocation)
        if (pagination.data.length > 0) setOffers(pagination.data)
      } catch (e) {
        props.error('Impossible de contacter le server')
        console.error(e)
      } finally {
        props.loaded()
      }
    }

    perform()
  }, [props])


  useEffect(() => {
    loadOffers()
  }, [loadOffers])

  return (
    <IndexContainer>
      <Background>
        <Menu />
        <OfferContainer>
          <div>
            <Title variant="h3" align="center">Venez aider ceux qui en ont le plus besoin</Title>
            <SubTitle variant="h5" align="center" gutterBottom>CHU ? Mairie ? Retraités ? Ils ont tous besoin de vous</SubTitle>
            <Hidden smDown>
              <Spacer />
              <OnlyEnoughHeight>
                <Grid justify="center" container spacing={2}>
                  <Grid item md={6}>
                    {offers.length > 0 && <CardAnnonce onToogled={loadOffers} offer={offers[0]}></CardAnnonce>}
                  </Grid>
                </Grid>
              </OnlyEnoughHeight>
            </Hidden>
          </div>
        </OfferContainer>
        <More>
          <h3>Découvrez les besoins</h3>
          <BounceIcon icon={faArrowDown} />
        </More>
      </Background>
      <BodyContainer>
        <AnnonceList />
      </BodyContainer>
      <Footer />
    </IndexContainer>
  )
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
    geolocation: state.geolocation,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    error: (message: string) => dispatch(error(message)),
  }
}

const Index = connect(mapStateToProps, mapDispatchToProps)(InternalIndex)

export default Index
