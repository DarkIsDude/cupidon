import React, { useState } from 'react'
import { TextField, CardActions, Divider, FormGroup, FormControlLabel, Switch, Typography } from '@material-ui/core'
import Button from '../../components/atoms/button'
import Menu from '../../components/organisms/menu'
import { Link } from 'react-router-dom'
import { CardContainer, CardContentColumn, Container, ExtentedCard } from '../../components/templates/single-card'
import styled from 'styled-components'
import { error, loaded, loading, State, success, API as IAPI } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { COOKIES_NAME, ONE_WEEK_IN_SECONDS } from 'cupidon-common/api'
import { useCookies } from 'react-cookie'

const Spacer = styled.div`
  margin: 2rem;
`

interface Props {
  api: IAPI;
  loaded: () => void;
  loading: () => void;
  error: (message: string) => {};
  success: (message: string) => {};
}

function InternalSignin(props: Props) {
  const [email, setEmail] = useState<string>()
  const [password, setPassword] = useState<string>()
  const [passwordConfirm, setPasswordConfirm] = useState<string>()
  const [prefix, setPrefix] = useState<string>()
  const [firstname, setFirstname] = useState<string>()
  const [lastname, setLastname] = useState<string>()
  const [phone, setPhone] = useState<string>()
  const setCookie = useCookies([COOKIES_NAME])[1]

  const [passwordMissmatch, setPasswordMissmatch] = useState<boolean>()
  const [emptyField, setEmptyField] = useState<boolean>()

  const signup = async () => {
    if (password !== passwordConfirm) {
      return setPasswordMissmatch(true)
    }

    setPasswordMissmatch(false)
    if (!email || !password || !prefix || !firstname || !lastname || !phone) {
      return setEmptyField(true)
    }

    setEmptyField(false)
    props.loading()

    try {
      const api = await props.api.signup(email, password, prefix, firstname, lastname, phone)
      props.success('Bienvenu !')
      setCookie('cupidon-token', api.token, { path: '/', maxAge: ONE_WEEK_IN_SECONDS })
    } catch (e) {
      console.error(e)

      if (e.isAxiosError && e.response) {
        const keys = Object.keys(e.response.data.error)
        const error = e.response.data.error[keys[0]][0]
        props.error(`Erreur lors de votre inscription: ${error}`)
      } else {
        props.error('Erreur lors de votre inscription')
      }
    } finally {
      props.loaded()
    }
  }

  return (
    <Container>
      <Menu />
      <CardContainer>
        <ExtentedCard variant="outlined">
          <form>
            <CardContentColumn>
              <TextField error={emptyField && !email} fullWidth required id="email" label="Email" type="email" value={email} onChange={e => setEmail(e.target.value)} />
              <TextField error={passwordMissmatch || (emptyField && !password)} fullWidth required id="password" label="Mot de passe" type="password" value={password} onChange={e => setPassword(e.target.value)} />
              <TextField error={passwordMissmatch} fullWidth required id="password-confirmation" label="Confirmation mot de passe" type="password" value={passwordConfirm} onChange={e => setPasswordConfirm(e.target.value)} />
              <Spacer />
              <FormGroup row>
                <FormControlLabel
                  control={<Switch color='primary' checked={prefix === 'M'} onChange={() => setPrefix('M')} name="prefixM" />}
                  label={<Typography color={emptyField && !prefix ? 'error' : 'initial'}>{'M.'}</Typography>}
                />
                <FormControlLabel
                  control={<Switch color='primary' checked={prefix === 'Mme'} onChange={() => setPrefix('Mme')} name="prefixMme" />}
                  label={<Typography color={emptyField && !prefix ? 'error' : 'initial'}>{'Mme.'}</Typography>}
                />
              </FormGroup>
              <TextField error={emptyField && !firstname} fullWidth required id="firstname" label="Prénom" value={firstname} onChange={e => setFirstname(e.target.value)} />
              <TextField error={emptyField && !lastname} fullWidth required id="lastname" label="Nom" value={lastname} onChange={e => setLastname(e.target.value)} />
              <TextField error={emptyField && !phone} fullWidth required id="lastname" label="Téléphone" value={phone} onChange={e => setPhone(e.target.value)} />
            </CardContentColumn>
            <CardActions>
              <Button color="primary" onClick={signup}>
                S'inscrire
              </Button>
              <Divider orientation="vertical" flexItem />
              <Button component={Link} to="/login">
                Se connecter
              </Button>
            </CardActions>
          </form>
        </ExtentedCard>
      </CardContainer>
    </Container>
  )
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    error: (message: string) => dispatch(error(message)),
    success: (message: string) => dispatch(success(message)),
  }
}

const Signin = connect(mapStateToProps, mapDispatchToProps)(InternalSignin)

export default Signin
