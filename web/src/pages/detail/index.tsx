import React, { ReactElement, useEffect, useState } from 'react'
import { Avatar, Card, CardContent, CardHeader, Chip, Container as MUIContainer, Divider, Grid, Hidden, Typography } from '@material-ui/core'
import Menu from '../../components/organisms/menu'
import { connect } from 'react-redux'
import { API, State, User, loading, loaded, error, success, Offer, Geolocation } from 'cupidon-common/state'
import { Dispatch } from 'redux'
import { Container } from '../../components/templates/single-card'
import { Redirect, useParams } from 'react-router-dom'
import Button from '../../components/atoms/button'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBuilding, faCalendarAlt, faClock, faHandshake, faUser } from '@fortawesome/free-solid-svg-icons'
import Distance from '../../components/atoms/distance'
import Footer from '../../components/organisms/footer'

const SpacerDivider = styled(Divider)`
  margin: 3rem;
`

const BottomSpacer = styled.div`
  height: 2rem;
`

const SecondaryAvatar = styled(Avatar)`
  background-color: ${({theme}) => theme.palette.secondary.main};
`

const CardMedia = styled.img`
  width: 100%;
`

const ApplyContainer = styled(Typography)`
  text-align: center;
  padding: 2rem;
`

interface Props {
  user?: User;
  api: API;
  geolocation: Geolocation;
  loaded: () => void;
  loading: () => void;
  error: (message: string) => {};
  success: (message: string) => {};
}

function InternalDetail(props: Props): ReactElement {
  const { loading, loaded, error, success, api, geolocation, user } = props
  const { id } = useParams<{ id?: string }>()
  const [offer, setOffer] = useState<Offer|null>()
  const [redirect, setRedirect] = useState<boolean>()

  useEffect(() => {
    const loadOffer = async () => {
      try {
        loading()
        setOffer(await api.offer(parseInt(id ? id : '0'), geolocation))
      } catch (e) {
        error('Erreur')
        console.error(e)
      } finally {
        loaded()
      }
    }

    loadOffer()
  }, [geolocation, api, loading, loaded, error, id])

  if (redirect) {
    return <Redirect to="/login" />
  }

  if (!offer) {
    return <></>
  }

  const toggleApply = async () => {
    if (!user) {
      return setRedirect(true)
    }

    try {
      loading()

      if (offer.auth_user_applied) {
        await api.detach(offer)
        success('Votre participation a été retiré')
      } else {
        await api.attach(offer)
        success('Votre participation a été enregistré')
      }

      offer.auth_user_applied = !offer.auth_user_applied
      setOffer(offer)
    } catch (e) {
      error(`${offer.name} : Impossible de contacter le server`)
      console.error(e)
    } finally {
      loaded()
    }
  }

  const renderPhone = () => {
    let rendered = <></>

    if (offer.establishment.phone1) rendered = <Chip label={offer.establishment.phone1} />
    if (offer.establishment.phone2) rendered = <>{rendered} <Chip label={offer.establishment.phone2} /></>

    return rendered
  }

  return (
    <Container>
      <Menu />
      <MUIContainer fixed>
        <Hidden xsDown>
          <Typography color="primary" gutterBottom={true} component="h1" variant="h2" align="center">
            {offer.isExpired() && '[TERMINEE] '}
            {offer.name}
          </Typography>
        </Hidden>
        <Hidden smUp>
          <Typography color="primary" gutterBottom={true} component="h1" variant="h4" align="center">
            {offer.isExpired() && '[TERMINEE] '}
            {offer.name}
          </Typography>
        </Hidden>
        <Typography gutterBottom={true} component="h2" variant="h5" align="center">
          <FontAwesomeIcon icon={faCalendarAlt} /> {offer.start.toLocaleDateString()} à {offer.startTime()} <FontAwesomeIcon icon={faClock} /> {offer.durationToString()}
        </Typography>

        <ApplyContainer>
          <Button onClick={toggleApply} color={offer.auth_user_applied ? 'secondary' : 'primary'} variant="contained" size="large" disabled={user && !offer.auth_user_can_apply}>
            {offer.auth_user_applied ? 'Ne plus participer' : 'Participer'}
          </Button> {(offer.max > 0 && offer.answers_count >= offer.max) && <Chip label={'Il n\' y a plus de place disponible'} color="secondary" />}
        </ApplyContainer>

        <SpacerDivider />

        <Typography gutterBottom={true} align="center" component="div">{offer.addressToString()}{offer.distance && <> est à <Distance offer={offer} /> de vous</>}</Typography>
        <iframe
          title="map"
          src={`https://maps.google.com/maps?q=${encodeURI(offer.addressToString())}&z=10&output=embed`}
          width="100%"
          height="400"
          frameBorder="0"
          style={{ border: 0 }}
          allowFullScreen={false}
          aria-hidden="false"
          tabIndex={0}></iframe>

        <Typography align="justify" component="div">
          <Chip
            icon={<FontAwesomeIcon icon={faHandshake} />}
            label={`${offer.participation()} place${offer.answers_count > 1 ? 's' : ''} déjà utilisée${offer.answers_count > 1 ? 's' : ''}`}
          />  {offer.description}
        </Typography>

        <SpacerDivider />

        <Grid container spacing={3} direction="row" justify="center" alignItems="center">
          <Grid item md={4}>
            <Card>
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  Etablissement
                </Typography>
              </CardContent>
              <CardHeader
                avatar={<SecondaryAvatar><FontAwesomeIcon icon={faBuilding} /></SecondaryAvatar>}
                title={offer.establishment.name}
                subheader={offer.addressToString()}
              />
              {offer.images && offer.images.length >= 1 && <CardMedia src={offer.images[0].url} alt={offer.establishment.name} />}
            </Card>
          </Grid>
          <Grid item md={4}>
            <Card>
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  Publié par
                </Typography>
              </CardContent>
              <CardHeader
                avatar={<SecondaryAvatar><FontAwesomeIcon icon={faUser} /></SecondaryAvatar>}
                title={`${offer.user.firstname} ${offer.user.lastname}`}
                subheader={renderPhone()}
              />
              {offer.images && offer.images.length >= 2 && <CardMedia src={offer.images[1].url} alt={offer.establishment.name} />}
            </Card>
          </Grid>
        </Grid>

        <BottomSpacer />
      </MUIContainer>
      <Footer />
    </Container>
  )
}

const mapStateToProps = (state: State) => {
  return {
    user: state.user,
    api: state.api,
    geolocation: state.geolocation,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    error: (message: string) => dispatch(error(message)),
    success: (message: string) => dispatch(success(message)),
  }
}

const Detail = connect(mapStateToProps, mapDispatchToProps)(InternalDetail)

export default Detail
