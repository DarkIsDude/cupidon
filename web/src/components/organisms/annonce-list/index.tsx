import React, { useState, useEffect } from 'react'
import { Divider, Grid, IconButton } from '@material-ui/core'
import { OfferLight, State, loading, loaded, error, Pagination, API, Geolocation } from 'cupidon-common/state'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import styled from 'styled-components'
import CardAnnonce from '../../molecules/card-annonce'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'

const PaginationContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const Spacer = styled(Divider)`
  margin: 4rem;
`

const GridCard = styled(Grid)`
  padding: 2rem;
`

interface Props {
  api: API
  geolocation: Geolocation
  loading: () => void
  loaded: () => void
  error: (message: string) => {}
}

function InternalAnnonceList(props: Props) {
  const [pagination, setPagination] = useState<Pagination<OfferLight>>({ current_page: 1, data: [], last_page: 1, total: 0 })

  useEffect(() => {
    async function perform() {
      try {
        props.loading()
        scrollTo()
        setPagination(await props.api.offers(pagination.current_page, props.geolocation))
      } catch (e) {
        props.error('Impossible de contacter le server')
        console.error(e)
      } finally {
        props.loaded()
      }
    }

    perform()
  }, [pagination.current_page, props])

  const next = () => {
    setPagination({ current_page: pagination.current_page + 1, data: [], last_page: pagination.last_page, total: pagination.total })
  }

  const before = () => {
    setPagination({ current_page: pagination.current_page - 1, data: [], last_page: pagination.last_page, total: pagination.total })
  }

  const scrollTo = () => {
    var interval: number = 0
    const step = 20

    function scrollToStep() {
      const element: HTMLElement | null = document.getElementById('top')
      if (!element) return clearInterval(interval)

      const target = element.offsetTop
      const current = window.scrollY
      const diff = current - target

      if (diff === 0 || current === 0) {
        clearInterval(interval)
      } else if (Math.abs(diff) < step) {
        window.scrollTo(0, target)
      } else if (diff > 0) {
        window.scrollTo(0, current - step)
      } else if (diff < 0) {
        window.scrollTo(0, current + step)
      }
    }

    interval = setInterval(scrollToStep, 1)
  }

  return (
    <>
      <Grid justify="center" container id="top">
        {pagination.data.map(offer => <GridCard key={offer.id} item md={6} xs={12}><CardAnnonce offer={offer}></CardAnnonce></GridCard>)}
      </Grid>
      <Spacer />
      <PaginationContainer>
        <IconButton color="primary" onClick={before} disabled={!pagination.prev_page_url}>
          <FontAwesomeIcon icon={faArrowLeft} />
        </IconButton>
        <h3>{pagination.current_page} / {pagination.last_page}</h3>
        <IconButton color="primary" onClick={next} disabled={!pagination.next_page_url}>
          <FontAwesomeIcon icon={faArrowRight} />
        </IconButton>
      </PaginationContainer>
    </>
  )
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
    geolocation: state.geolocation,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    error: (message: string) => dispatch(error(message)),
  }
}

const AnnonceList = connect(mapStateToProps, mapDispatchToProps)(InternalAnnonceList)

export default AnnonceList
