import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import Button from '../../atoms/button'
import { Grid } from '@material-ui/core'

const Container = styled(Grid)`
  margin-top: 1rem;
  padding-top: 2rem;
  padding-bottom: 2rem;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.palette.grey['200']};
`

function Footer() {
  return <Container spacing={5}>
    <Grid item>
      <Button href="/admin">Professionnel</Button>
    </Grid>
    <Grid item>
      <Button component={Link} to="/cgu">CGU</Button>
    </Grid>
    <Grid item>
      <Button href="mailto:q.estrade@orange.fr">Contact</Button>
    </Grid>
  </Container>
}

export default Footer
