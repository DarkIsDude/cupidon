import React from 'react'
import Button from '../../atoms/button'
import styled from 'styled-components'
import Box from '@material-ui/core/Box'
import { Hidden, MenuItem, Menu as MUIMenu } from '@material-ui/core'
import { connect } from 'react-redux'
import { User, State, logout, success } from 'cupidon-common/state'
import { Link, Redirect } from 'react-router-dom'
import { Dispatch } from 'redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignInAlt, faSignOutAlt, faBars, faUser } from '@fortawesome/free-solid-svg-icons'
import { useCookies } from 'react-cookie'
import { COOKIES_NAME } from 'cupidon-common/api'
import Logo from './logo.png'

const ImgLogo = styled.img`
  max-height: 3rem;
`

const MainMenu = styled.menu`
  margin: 0;
  padding: 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${({ theme }) => theme.palette.background.default};
  border-bottom: 1px solid ${({ theme }) => theme.palette.primary.main};
`

const Div = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
`

interface Props {
  user?: User;
  logout: () => void;
  success: (message: string) => {};
}

function InternalMenu(props: Props) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const removeCookie = useCookies([COOKIES_NAME])[2]

  const openMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const closeMenu = () => {
    setAnchorEl(null)
  }

  const logout = () => {
    props.logout()
    props.success('Déconnecté')
    removeCookie(COOKIES_NAME)

    return <Redirect to='/' />
  }

  let desktopUserMenu = <>
    <Box m={1}>
      <Button component={Link} to="/login" startIcon={<FontAwesomeIcon icon={faUser} />}>Se connecter</Button>
    </Box>
    <Box m={1}>
      <Button component={Link} to="/signin" startIcon={<FontAwesomeIcon icon={faSignInAlt} />}>S'inscrire</Button>
    </Box>
  </>
  let mobileUserMenu = <>
    <MenuItem component={Link} to="/login">Se connecter</MenuItem>
    <MenuItem component={Link} to="/signin">S'inscrire</MenuItem>
  </>

  if (props.user) {
    desktopUserMenu = <Box m={1}><Button onClick={logout} startIcon={<FontAwesomeIcon icon={faSignOutAlt} />}>Se déconnecter</Button></Box>
    mobileUserMenu = <MenuItem onClick={logout}>Se déconnecter</MenuItem>
  }

  return (
    <MainMenu>
      <Div>
        <Button color="primary" component={Link} to="/" startIcon={<ImgLogo src={Logo} alt={'La mêlée contre l\'isolement'} />}>La mêlée contre l'isolement</Button>
      </Div>
      <Hidden xsDown>
        <Div>
          {desktopUserMenu}
        </Div>
      </Hidden>
      <Hidden smUp>
        <Button aria-controls="main-menu" aria-haspopup="true" onClick={openMenu}>
          <FontAwesomeIcon icon={faBars} />
        </Button>
        <MUIMenu
          id="main-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={closeMenu}
        >
          <MenuItem component={Link} to="/">Annonces</MenuItem>
          {mobileUserMenu}
        </MUIMenu>
      </Hidden>
    </MainMenu>
  )
}

const mapStateToProps = (state: State) => {
  return {
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    logout: () => dispatch(logout()),
    success: (message: string) => dispatch(success(message)),
  }
}

const Menu = connect(mapStateToProps, mapDispatchToProps)(InternalMenu)

export default Menu
