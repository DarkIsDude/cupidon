import React, { useState } from 'react'
import { connect } from 'react-redux'
import { geolocated, Geolocation as GeolocationState } from 'cupidon-common/state'
import { Dispatch } from 'redux'

interface Props {
  geolocated: (lat: number, lon: number) => void;
}

function GeolocationInternal(props: Props) {
  const [geolocation, setGeolocation] = useState<GeolocationState>()

  if (navigator.geolocation && (!geolocation || !geolocation.lat || !geolocation.lon)) {
    navigator.geolocation.getCurrentPosition((position) => {
      props.geolocated(position.coords.latitude, position.coords.longitude)
      setGeolocation({ lat: position.coords.latitude, lon: position.coords.longitude })
    })
  }

  return (
    <></>
  )
}

const mapStateToProps = () => ({})

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    geolocated: (lat: number, lon: number) => dispatch(geolocated(lat, lon)),
  }
}

const Geolocation = connect(mapStateToProps, mapDispatchToProps)(GeolocationInternal)

export default Geolocation
