import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { useCookies } from 'react-cookie'
import ShareApple from './share-apple-rounded.png'
import Button from '../../atoms/button'

const PWA_COOKIE = 'pwa-banner'
const ONE_MONTH_IN_SECONDS = 2629746

const Banner = styled.div`
  position: absolute;
  bottom: 0;
  height: 8rem;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${({theme}) => theme.palette.primary.main};
  z-index: 1;
  margin: 1rem;
  text-align: center;
  flex-direction: column;
`

const Img = styled.img`
  width: 1rem;
`

function PWABanner() {
  const [cookies, setCookie] = useCookies([PWA_COOKIE])
  const [isIOS, setIsIOS] = useState<boolean>(false)

  useEffect(() => {
    if ([
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod'
    ].includes(navigator.platform)) {
      setIsIOS(true)
    }
  }, [isIOS])

  if (!isIOS || cookies[PWA_COOKIE]) {
    return <></>
  }

  return (
    <Banner>
      <span>Vous pouvez installer l'application en cliquant sur <Img src={ShareApple} alt="Apple share button" /> puis sur "Ajouter à l'écran d'accueil"</span>
      <Button variant="outlined" onClick={() => setCookie(PWA_COOKIE, true, { path: '/', maxAge: ONE_MONTH_IN_SECONDS })}>Merci</Button>
    </Banner>
  )
}

export default PWABanner
