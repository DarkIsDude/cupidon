import styled from 'styled-components'
import { CardContent, Card } from '@material-ui/core'

export const Container = styled.div`
  min-height: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`

export const CardContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
`

export const CardContentColumn = styled(CardContent)`
  display: flex;
  flex-direction: column;
`

export const ExtentedCard = styled(Card)`
  min-width: 30%;
`
