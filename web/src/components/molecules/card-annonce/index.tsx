import React from 'react'
import Button from '../../atoms/button'
import { Card, CardContent, CardActions as MUICardActions, List, ListItem, ListItemIcon, ListItemText, CardHeader, Typography } from '@material-ui/core'
import { error, loaded, loading, OfferLight, State, success, User, API } from 'cupidon-common/state'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt, faClock, faHandshake, faMap } from '@fortawesome/free-solid-svg-icons'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Distance from '../../atoms/distance'

const CardHeaderColor = styled(CardHeader)`
  background-color: ${({ theme, color }) => color === 'primary' ? theme.palette.primary.main : theme.palette.primary.light};
  color: ${({ theme, color }) => color === 'primary' ? theme.palette.primary.contrastText : theme.palette.primary.contrastText};
`

const CardActions = styled(MUICardActions)`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const TransparentLink = styled(Link)`
  text-decoration: none;
`

interface Props {
  offer: OfferLight;
  user?: User;
  api: API;
  onToogled?: () => void;
  loading: () => void;
  loaded: () => void;
  error: (message: string) => {};
  success: (message: string) => {};
}

function InternalCardAnnonce(props: Props) {
  if (!props.offer) {
    return <></>
  }

  return (
    <Card>
      <TransparentLink to={`/offer/${props.offer.id}`}>
        <CardHeaderColor
          color={props.offer.isExpired() ? 'secondary' : 'primary'}
          title={<Typography align="center" variant="h6">{props.offer.isExpired() && '[TERMINEE] '}{props.offer.name}</Typography>}
          subheader={<Typography align="center">
            <FontAwesomeIcon icon={faCalendarAlt} />&nbsp;
            {props.offer.start.toLocaleDateString()}&nbsp;
            <FontAwesomeIcon icon={faClock} />&nbsp;
            {props.offer.durationToString()}
          </Typography>}
        />
      </TransparentLink>

      <CardContent>
        <List>
          <ListItem button>
            <ListItemIcon>
              <FontAwesomeIcon icon={faMap} />
            </ListItemIcon>
            <ListItemText primary={props.offer.addressToString()} />
          </ListItem>
        </List>
      </CardContent>

      <CardActions>
        <Button to={`/offer/${props.offer.id}`} component={Link} color={props.offer.auth_user_applied ? 'secondary' : 'primary'}>
          {props.offer.participation()} &nbsp; <FontAwesomeIcon icon={faHandshake} /> &nbsp; En savoir plus
        </Button>
        <Distance offer={props.offer} />
      </CardActions>
    </Card>
  )
}

const mapStateToProps = (state: State) => {
  return {
    api: state.api,
    user: state.user,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loading: () => dispatch(loading()),
    loaded: () => dispatch(loaded()),
    error: (message: string) => dispatch(error(message)),
    success: (message: string) => dispatch(success(message)),
  }
}

const CardAnnonce = connect(mapStateToProps, mapDispatchToProps)(InternalCardAnnonce)

export default CardAnnonce
