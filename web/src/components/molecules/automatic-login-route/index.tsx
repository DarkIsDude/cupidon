import React, { ComponentType } from 'react'
import { State, User } from 'cupidon-common/state'
import { connect } from 'react-redux'
import { Redirect, Route, RouteProps } from 'react-router-dom'
import { useCookies } from 'react-cookie'
import { COOKIES_NAME } from 'cupidon-common/api'

interface Props extends RouteProps {
  user?: User;
  path: string;
  component: ComponentType;
}

function InternalAutomaticLoginRoute(props: Props) {
  const [cookies] = useCookies([COOKIES_NAME])
  const { user, path } = props
  const Component = props.component

  return <Route path={path} render={() => {
    if (cookies[COOKIES_NAME] && !user) {
      return <Redirect to="/login" />
    }

    return <Component />
  }} />
}

const mapStateToProps = (state: State) => {
  return {
    user: state.user,
  }
}

const AutomaticLoginRoute = connect(mapStateToProps)(InternalAutomaticLoginRoute)

export default AutomaticLoginRoute
