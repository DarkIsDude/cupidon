import React from 'react'
import { IconButton, Snackbar, SnackbarContent } from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { Alert as AlertModel, AlertType } from 'cupidon-common/state'
import styled from 'styled-components'

interface Props {
  alert: AlertModel;
}

const ErrorMessage = styled(SnackbarContent)`
  background-color: ${({ theme }) => theme.palette.error.main};
`

const SuccessMessage = styled(SnackbarContent)`
  background-color: ${({ theme }) => theme.palette.success.main};
`

const InfoMessage = styled(SnackbarContent)`
  background-color: ${({ theme }) => theme.palette.info.main};
`

function Alert(props: Props) {
  const [open, setOpen] = React.useState(true)

  const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return
    }

    setOpen(false)
  }

  let Component = InfoMessage
  switch (props.alert.type) {
    case AlertType.Error:
      Component = ErrorMessage
      break
    case AlertType.Success:
      Component = SuccessMessage
      break
    default:
      Component = InfoMessage
      break
  }

  return <Snackbar
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    open={open}
    autoHideDuration={6000}
    onClose={handleClose}
  >
    <Component
      action={
        <IconButton size="small" onClick={handleClose}>
          <FontAwesomeIcon icon={faTimes} />
        </IconButton>
      }
      message={props.alert.message} />
  </Snackbar>
}

export default Alert
