import React from 'react'

import MaterialButton, { ButtonProps } from '@material-ui/core/Button'

interface Props extends ButtonProps {
  component?: any;
  to?: string;
}

function Button(props: Props) {
  return (
    <MaterialButton {...props}>{props.children}</MaterialButton>
  )
}

export default Button
