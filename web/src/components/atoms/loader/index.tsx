import React from 'react'

import { Backdrop, BackdropProps } from '@material-ui/core'
import styled from 'styled-components'

const BackdropTop = styled(Backdrop)`
  z-index: 1;
`

function Loader(props: BackdropProps) {
  return (
    <BackdropTop {...props}>{props.children}</BackdropTop>
  )
}

export default Loader
