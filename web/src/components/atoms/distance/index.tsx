import { faRoad } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Chip } from '@material-ui/core'
import React from 'react'
import { OfferLight } from 'cupidon-common/state'

interface Props {
  offer: OfferLight;
}

function Distance(props: Props) {
  if (!props.offer.distance) {
    return <></>
  }

  const distance = Math.round(props.offer.distance)

  return <Chip icon={<FontAwesomeIcon icon={faRoad} />} label={`${distance}km`} color={distance < 50 ? 'secondary' : 'primary'} />
}

export default Distance
