# Cupidon

You can find the iOS and Android in the native folder. The website can be found in web.

## Commit message

Each commit should be comited with this format: `type(<ticket number>): message`. For example `feat(#1): what I'm doing`.

The type can be `feat`, `doc`, `test`, `core`, `pipeline`, `fix`, `refactor`.

## Test and pipeline

Each project have a pipelie and linter, please respect them. We use 2 space as indentation.
